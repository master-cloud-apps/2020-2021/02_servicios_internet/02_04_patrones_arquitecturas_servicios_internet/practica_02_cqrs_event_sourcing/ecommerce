package org.eyo.architecture.hexagonal.ecommerce.cartexpenditure.domain.port;

import org.eyo.architecture.hexagonal.ecommerce.cartexpenditure.domain.dto.ShoppingCartExpenditureDTO;
import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.dto.ShoppingCartDTO;

import java.util.List;

public interface ShoppingCartExpenditureUseCase {
    ShoppingCartDTO save(ShoppingCartDTO cart);

    List<ShoppingCartExpenditureDTO> getExpenditures();
}
