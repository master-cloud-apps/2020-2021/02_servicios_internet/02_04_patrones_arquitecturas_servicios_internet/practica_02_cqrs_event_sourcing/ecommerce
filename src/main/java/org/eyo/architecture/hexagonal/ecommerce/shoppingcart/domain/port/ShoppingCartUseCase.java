package org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.port;

import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.dto.ShoppingCartDTO;
import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.dto.CreateShoppingCartDTO;

public interface ShoppingCartUseCase {
    ShoppingCartDTO createShoppingCart(CreateShoppingCartDTO shoppingCartInput);

    ShoppingCartDTO endCart(Long cartId);

    ShoppingCartDTO getShoppingCart(Long cartId);

    ShoppingCartDTO addProductToShoppingCart(Long cartId, Long productId, Integer quantity);

    ShoppingCartDTO deleteProductFromCart(Long cartId, Long productId);

    ShoppingCartDTO deleteCart(Long cartId);
}
