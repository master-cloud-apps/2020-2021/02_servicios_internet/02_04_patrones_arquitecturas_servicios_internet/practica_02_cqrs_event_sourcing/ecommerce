package org.eyo.architecture.hexagonal.ecommerce.product.infraestructure.repository;

import org.eyo.architecture.hexagonal.ecommerce.product.domain.dto.ProductDTO;
import org.eyo.architecture.hexagonal.ecommerce.product.domain.port.ProductRepository;
import org.eyo.architecture.hexagonal.ecommerce.product.infraestructure.model.ProductEntity;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class ProductDBRepository implements ProductRepository {

    private ProductJpaRepository productJpaRepository;
    private ModelMapper modelMapper;

    public ProductDBRepository(ProductJpaRepository productJpaRepository, ModelMapper modelMapper) {
        this.productJpaRepository = productJpaRepository;
        this.modelMapper = modelMapper;
    }

    @Override
    public ProductDTO save(ProductDTO productDTO) {
        ProductEntity productSaved = this.productJpaRepository.save(this.modelMapper.map(productDTO,
                ProductEntity.class));
        return modelMapper.map(productSaved, ProductDTO.class);
    }

    @Override
    public ProductDTO findById(Long productId) {
        Optional<ProductEntity> productFound = this.productJpaRepository.findById(productId);
        if (!productFound.isPresent())
            return null;
        return this.modelMapper.map(productFound.get(), ProductDTO.class);
    }

    @Override
    public ProductDTO deleteById(Long productId) {
        ProductDTO productToDelete = this.findById(productId);
        this.productJpaRepository.deleteById(productId);
        return productToDelete;
    }

    @Override
    public List<ProductDTO> getProducts() {
        return this.productJpaRepository.findAll()
                .stream()
                .map(productEntity -> this.modelMapper.map(productEntity, ProductDTO.class))
                .collect(Collectors.toList());
    }
}
