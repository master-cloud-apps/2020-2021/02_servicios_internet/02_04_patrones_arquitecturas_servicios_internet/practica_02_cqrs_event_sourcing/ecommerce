package org.eyo.architecture.hexagonal.ecommerce.shoppingcart.infraestructure.repository;

import org.eyo.architecture.hexagonal.ecommerce.cartexpenditure.domain.dto.ShoppingCartExpenditureDTO;
import org.eyo.architecture.hexagonal.ecommerce.cartexpenditure.infraestructure.model.ShoppingCartExpenditureEntity;
import org.eyo.architecture.hexagonal.ecommerce.cartexpenditure.infraestructure.repository.ShoppingCartExpenditureJpaRepository;
import org.eyo.architecture.hexagonal.ecommerce.product.domain.dto.ProductDTO;
import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.dto.CartItemDTO;
import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.dto.ShoppingCartDTO;
import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.port.ShoppingCartRepository;
import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.infraestructure.model.CartItemEntity;
import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.infraestructure.model.ShoppingCartEntity;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class ShoppingCartDBRepository implements ShoppingCartRepository {
    private ShoppingCartJpaRepository shoppingCartJpaRepository;
    private ShoppingCartExpenditureJpaRepository shoppingCartExpenditureJpaRepository;
    private ModelMapper modelMapper;

    public ShoppingCartDBRepository(ShoppingCartJpaRepository shoppingCartJpaRepository, ModelMapper modelMapper,
                                    ShoppingCartExpenditureJpaRepository shoppingCartExpenditureJpaRepository) {
        this.shoppingCartJpaRepository = shoppingCartJpaRepository;
        this.modelMapper = modelMapper;
        this.shoppingCartExpenditureJpaRepository = shoppingCartExpenditureJpaRepository;
    }

    @Override
    public ShoppingCartDTO save(ShoppingCartDTO shoppingCartDTO) {
        ShoppingCartEntity shoppingCartSaved = this.shoppingCartJpaRepository.save(this.modelMapper.map(shoppingCartDTO,
                ShoppingCartEntity.class));
        return this.mapEntity(shoppingCartSaved);
    }

    @Override
    public ShoppingCartDTO findById(Long shoppingCartId) {
        Optional<ShoppingCartEntity> cartFound = this.shoppingCartJpaRepository.findById(shoppingCartId);
        if (!cartFound.isPresent())
            return null;
        return this.mapEntity(cartFound.get());
    }

    @Override
    public ShoppingCartDTO deleteById(Long shoppingCartId) {
        ShoppingCartDTO cartToDelete = this.findById(shoppingCartId);
        this.shoppingCartJpaRepository.deleteById(shoppingCartId);
        return cartToDelete;
    }

    @Override
    public ShoppingCartExpenditureDTO saveExpenditure(ShoppingCartExpenditureDTO shoppingCart) {
        ShoppingCartExpenditureEntity entityToSave = this.modelMapper.map(shoppingCart,
                ShoppingCartExpenditureEntity.class);
        Optional<ShoppingCartEntity> cartEntity = this.shoppingCartJpaRepository.findById(shoppingCart.getCartId());
        if (cartEntity.isPresent()) {
            entityToSave.setShoppingCart(cartEntity.get());
            this.shoppingCartExpenditureJpaRepository.save(entityToSave);
        }
        return this.modelMapper.map(entityToSave, ShoppingCartExpenditureDTO.class);
    }

    @Override
    public List<ShoppingCartExpenditureDTO> getExpenditures() {
        return this.shoppingCartExpenditureJpaRepository.findAll()
                .stream()
                .map(expenditure -> this.modelMapper.map(expenditure, ShoppingCartExpenditureDTO.class))
                .collect(Collectors.toList());
    }

    private ShoppingCartDTO mapEntity(ShoppingCartEntity entity) {
        ShoppingCartDTO result = this.modelMapper.map(entity, ShoppingCartDTO.class);
        if (entity.getCartItems() != null) {
            result.setCartItems(entity.getCartItems().stream().map(this::mapCartItemEntity).collect(Collectors.toList()));
        }
        return result;
    }

    private CartItemDTO mapCartItemEntity(CartItemEntity cartItemEntity) {
        ProductDTO productInResult = this.modelMapper.map(cartItemEntity.getProduct(), ProductDTO.class);
        return new CartItemDTO(productInResult, cartItemEntity.getQuantity());
    }
}
