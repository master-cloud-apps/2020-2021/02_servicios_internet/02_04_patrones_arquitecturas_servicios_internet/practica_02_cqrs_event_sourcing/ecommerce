package org.eyo.architecture.hexagonal.ecommerce.product.application.controller;

import org.eyo.architecture.hexagonal.ecommerce.product.application.service.ProductQueryService;
import org.eyo.architecture.hexagonal.ecommerce.product.domain.dto.ProductDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

import static org.eyo.architecture.hexagonal.ecommerce.utils.AppConstants.PRODUCT_URL;

@RestController
@RequestMapping(PRODUCT_URL)
public class ProductQueryController {

    private final ProductQueryService productService;

    public ProductQueryController(ProductQueryService productService) {
        this.productService = productService;
    }

    @GetMapping("")
    public ResponseEntity<Collection<ProductDTO>> getProducts() {
        return ResponseEntity.ok(this.productService.getProducts());
    }

    @GetMapping("/{productId}")
    public ResponseEntity<ProductDTO> getProductById(@PathVariable Long productId) {
        if (this.productService.getProduct(productId) == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(this.productService.getProduct(productId));
    }


}
