package org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain;

import org.eyo.architecture.hexagonal.ecommerce.product.domain.Product;
import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.dto.ShoppingCartDTO;
import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.port.EcommerceManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class ShoppingCart {
    private Long id;
    private ShoppingCartStatus status;
    private List<CartItem> cartItems;
    private double expenditure;

    public ShoppingCart(ShoppingCartDTO shoppingCartDTO) {
        this.id = shoppingCartDTO.getId();
        this.status = ShoppingCartStatus.valueOf(shoppingCartDTO.getStatus());
        this.cartItems = this.mapItemsFromDTO(shoppingCartDTO);
    }

    public ShoppingCart() {
        this.cartItems = new ArrayList<>();
        this.status = ShoppingCartStatus.PENDING;
    }

    private List<CartItem> mapItemsFromDTO(ShoppingCartDTO shoppingCartDTO){
        if (shoppingCartDTO.getCartItems() != null){
            return shoppingCartDTO.getCartItems().stream().map(CartItem::new).collect(Collectors.toList());
        }
        return new ArrayList<>();
    }

    public ShoppingCart includeProductInQuantity(Product productToInclude, Integer quantity) {
        if (this.status.equals(ShoppingCartStatus.COMPLETED)){
            return this;
        }
        if (this.findCartItemByProductId(productToInclude).isPresent()) {
            this.findCartItemByProductId(productToInclude).get().setQuantity(quantity);
            return this;
        }
        this.cartItems.add(new CartItem(productToInclude, quantity));
        return this;
    }

    private Optional<CartItem> findCartItemByProductId(Product product) {
        return this.cartItems.stream().filter(cartItem -> cartItem.getProductId().equals(product.getId())).findFirst();
    }

    public Long getId() {
        return id;
    }

    public ShoppingCartStatus getStatus() {
        return status;
    }

    public List<CartItem> getCartItems() {
        return cartItems;
    }

    public ShoppingCart deleteProduct(Long productId) {
        this.cartItems =
                this.cartItems.stream().filter(item -> !item.getProductId().equals(productId)).collect(Collectors.toList());
        return this;
    }

    public ShoppingCartStatus validate(EcommerceManager cartValidator) {
        this.status = ShoppingCartStatus.NOT_VALIDATED;
        if (Boolean.TRUE.equals(cartValidator.validateCart(new ShoppingCartDTO(this)))){
            this.status = ShoppingCartStatus.COMPLETED;
        }
        return this.status;
    }

    public void calculateTotal() {
        this.expenditure = this.cartItems.stream()
                .reduce(0D,
                        (total, cartItem) -> total + cartItem.getProduct().getPrice(),
                        Double::sum);
    }

    public double getExpenditure() {
        return expenditure;
    }
}
