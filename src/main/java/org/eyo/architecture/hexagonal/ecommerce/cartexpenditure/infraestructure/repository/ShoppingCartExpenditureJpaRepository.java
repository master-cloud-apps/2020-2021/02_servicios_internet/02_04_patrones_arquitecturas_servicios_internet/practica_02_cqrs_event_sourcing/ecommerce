package org.eyo.architecture.hexagonal.ecommerce.cartexpenditure.infraestructure.repository;

import org.eyo.architecture.hexagonal.ecommerce.cartexpenditure.infraestructure.model.ShoppingCartExpenditureEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ShoppingCartExpenditureJpaRepository extends JpaRepository<ShoppingCartExpenditureEntity, Long> {
}
