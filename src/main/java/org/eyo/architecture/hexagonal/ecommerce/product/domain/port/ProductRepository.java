package org.eyo.architecture.hexagonal.ecommerce.product.domain.port;

import org.eyo.architecture.hexagonal.ecommerce.product.domain.dto.ProductDTO;

import java.util.List;

public interface ProductRepository {
    ProductDTO save(ProductDTO productDTO);

    ProductDTO findById(Long productId);

    ProductDTO deleteById(Long productId);

    List<ProductDTO> getProducts();
}
