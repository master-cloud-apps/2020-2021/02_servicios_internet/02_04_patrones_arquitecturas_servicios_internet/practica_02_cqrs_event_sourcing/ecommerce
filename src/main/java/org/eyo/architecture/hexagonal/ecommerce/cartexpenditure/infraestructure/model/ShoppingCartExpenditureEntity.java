package org.eyo.architecture.hexagonal.ecommerce.cartexpenditure.infraestructure.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.infraestructure.model.ShoppingCartEntity;

import javax.persistence.*;

@Entity(name = "CartExpenditure")
@NoArgsConstructor
@Getter
@Setter
public class ShoppingCartExpenditureEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @OneToOne
    private ShoppingCartEntity shoppingCart;

    private double expenditure;
}
