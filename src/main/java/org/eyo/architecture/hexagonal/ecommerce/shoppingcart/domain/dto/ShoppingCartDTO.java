package org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.dto;

import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.ShoppingCart;
import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.ShoppingCartStatus;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ShoppingCartDTO {

    private Long id;
    private List<CartItemDTO> cartItems;
    private String status;

    public ShoppingCartDTO() {
        this.cartItems = new ArrayList<>();
        this.status = ShoppingCartStatus.PENDING.name();
    }

    public ShoppingCartDTO(ShoppingCart cartToIncludeProduct) {
        this.id = cartToIncludeProduct.getId();
        this.cartItems = cartToIncludeProduct.getCartItems().stream().map(CartItemDTO::new).collect(Collectors.toList());
        this.status = String.valueOf(cartToIncludeProduct.getStatus());
    }

    public ShoppingCartDTO(CreateShoppingCartDTO shoppingCartInput) {
        this.id = shoppingCartInput.getId();
        this.status = shoppingCartInput.getStatus();
        this.cartItems = new ArrayList<>();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<CartItemDTO> getCartItems() {
        return cartItems;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setCartItems(List<CartItemDTO> cartItems) {
        this.cartItems = cartItems;
    }

}
