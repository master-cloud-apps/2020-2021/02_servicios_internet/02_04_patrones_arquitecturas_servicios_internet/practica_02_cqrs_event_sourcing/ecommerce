package org.eyo.architecture.hexagonal.ecommerce.product.application.service;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class DeleteProductDTO {
    private Long productId;
}
