package org.eyo.architecture.hexagonal.ecommerce.product.application.service;

import org.eyo.architecture.hexagonal.ecommerce.product.domain.dto.CreateProductRequestDTO;
import org.eyo.architecture.hexagonal.ecommerce.product.domain.dto.ProductDTO;

public interface ProductCommandService {
    void create(CreateProductRequestDTO createProductRequestDTO);

    ProductDTO deleteProduct(Long productId);

}
