package org.eyo.architecture.hexagonal.ecommerce.shoppingcart.application.service;

import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.dto.ShoppingCartDTO;

public interface ShoppingCartQueryService {

    ShoppingCartDTO getShoppingCart(Long cartId);

}
