package org.eyo.architecture.hexagonal.ecommerce.shoppingcart.infraestructure.repository;

import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.infraestructure.model.ShoppingCartEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ShoppingCartJpaRepository extends JpaRepository<ShoppingCartEntity, Long> {
}
