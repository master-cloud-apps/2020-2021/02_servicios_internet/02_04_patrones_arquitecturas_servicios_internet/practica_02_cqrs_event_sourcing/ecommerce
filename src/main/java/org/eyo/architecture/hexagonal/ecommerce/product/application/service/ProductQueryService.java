package org.eyo.architecture.hexagonal.ecommerce.product.application.service;

import org.eyo.architecture.hexagonal.ecommerce.product.domain.dto.ProductDTO;

import java.util.List;

public interface ProductQueryService {
    ProductDTO getProduct(Long productId);

    List<ProductDTO> getProducts();
}
