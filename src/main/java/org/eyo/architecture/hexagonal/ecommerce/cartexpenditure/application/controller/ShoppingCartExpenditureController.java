package org.eyo.architecture.hexagonal.ecommerce.cartexpenditure.application.controller;

import org.eyo.architecture.hexagonal.ecommerce.cartexpenditure.application.service.ShoppingCartExpenditureQueryService;
import org.eyo.architecture.hexagonal.ecommerce.cartexpenditure.domain.dto.ShoppingCartExpenditureDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

import static org.eyo.architecture.hexagonal.ecommerce.utils.AppConstants.SHOPPING_CART_EXPENDITURE_URL;

@RestController
@RequestMapping(SHOPPING_CART_EXPENDITURE_URL)
public class ShoppingCartExpenditureController {

    private ShoppingCartExpenditureQueryService shoppingCartExpenditureQueryService;

    public ShoppingCartExpenditureController(ShoppingCartExpenditureQueryService shoppingCartExpenditureQueryService) {
        this.shoppingCartExpenditureQueryService = shoppingCartExpenditureQueryService;
    }

    @GetMapping("")
    public ResponseEntity<Collection<ShoppingCartExpenditureDTO>> getExpenditures() {
        return ResponseEntity.ok(this.shoppingCartExpenditureQueryService.getExpenditures());
    }
}
