package org.eyo.architecture.hexagonal.ecommerce.product.domain;

import org.eyo.architecture.hexagonal.ecommerce.product.domain.dto.ProductDTO;

public class Product {
    private Long id;
    private String description;
    private String name;
    private String kind;
    private double price;

    public Product(ProductDTO productDTOSaved) {
        this.id = productDTOSaved.getId();
        this.description = productDTOSaved.getDescription();
        this.name = productDTOSaved.getName();
        this.kind = productDTOSaved.getKind();
        this.price = productDTOSaved.getPrice();
    }

    public Long getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public String getKind() {
        return this.kind;
    }

    public String getDescription() {
        return this.description;
    }

    public double getPrice() {
        return this.price;
    }
}
