package org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.port;


import org.eyo.architecture.hexagonal.ecommerce.cartexpenditure.domain.dto.ShoppingCartExpenditureDTO;
import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.dto.ShoppingCartDTO;

import java.util.List;

public interface ShoppingCartRepository {
    ShoppingCartDTO save(ShoppingCartDTO shoppingCartDTO);

    ShoppingCartDTO findById(Long shoppingCartId);

    ShoppingCartDTO deleteById(Long shoppingCartId);

    ShoppingCartExpenditureDTO saveExpenditure(ShoppingCartExpenditureDTO shoppingCart);

    List<ShoppingCartExpenditureDTO> getExpenditures();
}
