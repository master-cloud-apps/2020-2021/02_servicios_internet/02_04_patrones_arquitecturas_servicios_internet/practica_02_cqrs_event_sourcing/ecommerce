package org.eyo.architecture.hexagonal.ecommerce.cartexpenditure.application.service.sink;

import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.dto.ShoppingCartDTO;

public interface ShoppingCartExpenditureCommandService {

    void saveShoppingCartExpenditure(ShoppingCartDTO endedCart);
}
