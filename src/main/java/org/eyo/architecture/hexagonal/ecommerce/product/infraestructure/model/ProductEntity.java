package org.eyo.architecture.hexagonal.ecommerce.product.infraestructure.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name = "Product")
@NoArgsConstructor
@Getter
@Setter
public class ProductEntity {

    @Id
    private Long id;

    private String kind;
    private String name;
    private String description;
    private double price;


}
