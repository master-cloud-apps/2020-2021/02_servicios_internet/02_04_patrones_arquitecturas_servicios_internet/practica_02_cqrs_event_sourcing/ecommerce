package org.eyo.architecture.hexagonal.ecommerce.shoppingcart.application.controller;

import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.application.service.ShoppingCartQueryService;
import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.dto.ShoppingCartDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.eyo.architecture.hexagonal.ecommerce.utils.AppConstants.SHOPPING_CART_URL;

@RestController
@RequestMapping(SHOPPING_CART_URL)
public class ShoppingCartQueryController {

    private ShoppingCartQueryService shoppingCartQueryService;

    public ShoppingCartQueryController(ShoppingCartQueryService shoppingCartQueryService) {
        this.shoppingCartQueryService = shoppingCartQueryService;
    }

    @GetMapping("/{cartId}")
    public ResponseEntity<ShoppingCartDTO> getShoppingCartById(@PathVariable Long cartId) {
        if (this.shoppingCartQueryService.getShoppingCart(cartId) == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(this.shoppingCartQueryService.getShoppingCart(cartId));
    }

}
