package org.eyo.architecture.hexagonal.ecommerce.product.domain.dto;

import java.util.UUID;

public class CreateProductRequestDTO {
    private Long id = UUID.randomUUID().getMostSignificantBits() & Long.MAX_VALUE;
    private String kind;
    private String name;
    private String description;
    private double price;

    public CreateProductRequestDTO() {
    }

    public CreateProductRequestDTO(String kind, String name, String description) {
        this.kind = kind;
        this.name = name;
        this.description = description;
    }

    public CreateProductRequestDTO(String kind, String name, String description, double price) {
        this.kind = kind;
        this.name = name;
        this.description = description;
        this.price = price;
    }

    public String getKind() {
        return kind;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public double getPrice() {
        return price;
    }

    public Long getId() {
        return this.id;
    }
}
