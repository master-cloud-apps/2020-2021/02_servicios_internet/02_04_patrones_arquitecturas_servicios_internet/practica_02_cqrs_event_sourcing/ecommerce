package org.eyo.architecture.hexagonal.ecommerce.product.application.service;

import org.eyo.architecture.hexagonal.ecommerce.product.domain.dto.ProductDTO;
import org.eyo.architecture.hexagonal.ecommerce.product.domain.port.ProductRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductQueryServiceImpl implements ProductQueryService {
    private ProductRepository productRepository;

    public ProductQueryServiceImpl(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }


    @Override
    public ProductDTO getProduct(Long productId) {
        return this.productRepository.findById(productId);
    }

    @Override
    public List<ProductDTO> getProducts() {
        return this.productRepository.getProducts();
    }
}
