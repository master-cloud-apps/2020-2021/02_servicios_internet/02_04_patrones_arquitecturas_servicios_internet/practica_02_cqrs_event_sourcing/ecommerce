package org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.dto;

import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.ShoppingCartStatus;

import java.util.UUID;

public class CreateShoppingCartDTO {
    private String status;
    private Long id = UUID.randomUUID().getMostSignificantBits() & Long.MAX_VALUE;

    public CreateShoppingCartDTO() {
        this.status = ShoppingCartStatus.PENDING.name();
    }

    public CreateShoppingCartDTO(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public Long getId() {
        return id;
    }
}
