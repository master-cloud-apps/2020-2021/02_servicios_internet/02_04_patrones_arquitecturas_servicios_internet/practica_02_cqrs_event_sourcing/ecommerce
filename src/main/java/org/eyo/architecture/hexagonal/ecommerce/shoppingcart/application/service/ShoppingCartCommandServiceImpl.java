package org.eyo.architecture.hexagonal.ecommerce.shoppingcart.application.service;

import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.ShoppingCartStatus;
import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.dto.CreateShoppingCartDTO;
import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.dto.ShoppingCartDTO;
import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.port.ShoppingCartUseCase;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class ShoppingCartCommandServiceImpl implements ShoppingCartCommandService {
    private ShoppingCartUseCase shoppingCartUseCase;
    private final ApplicationEventPublisher applicationEventPublisher;

    public ShoppingCartCommandServiceImpl(ShoppingCartUseCase shoppingCartUseCase,
                                          ApplicationEventPublisher applicationEventPublisher) {
        this.shoppingCartUseCase = shoppingCartUseCase;
        this.applicationEventPublisher = applicationEventPublisher;
    }

    @Override
    public ShoppingCartDTO createShoppingCart(CreateShoppingCartDTO shoppingCartInput) {
        return this.shoppingCartUseCase.createShoppingCart(shoppingCartInput);
    }

    @Transactional
    @Override
    public ShoppingCartDTO endCart(Long cartId) {
        ShoppingCartDTO cartToEnd = this.shoppingCartUseCase.getShoppingCart(cartId);
        if (cartToEnd == null){
            return null;
        }
        if (cartToEnd.getStatus().equals(ShoppingCartStatus.COMPLETED.name())){
            return cartToEnd;
        }
        ShoppingCartDTO endedCart = this.shoppingCartUseCase.endCart(cartId);
        if (endedCart.getStatus().equals(ShoppingCartStatus.COMPLETED.name()))
            this.applicationEventPublisher.publishEvent(endedCart);
        return endedCart;
    }

    @Override
    public ShoppingCartDTO addProductToShoppingCart(Long cartId, Long productId, Integer quantity) {
        return this.shoppingCartUseCase.addProductToShoppingCart(cartId, productId, quantity);
    }

    @Override
    public ShoppingCartDTO deleteProductFromCart(Long cartId, Long productId) {
        return this.shoppingCartUseCase.deleteProductFromCart(cartId, productId);
    }

    @Override
    public ShoppingCartDTO deleteCart(Long cartId) {
        ShoppingCartDTO cartToEnd = this.shoppingCartUseCase.getShoppingCart(cartId);
        if (cartToEnd == null){
            return null;
        }
        return this.shoppingCartUseCase.deleteCart(cartId);
    }
}
