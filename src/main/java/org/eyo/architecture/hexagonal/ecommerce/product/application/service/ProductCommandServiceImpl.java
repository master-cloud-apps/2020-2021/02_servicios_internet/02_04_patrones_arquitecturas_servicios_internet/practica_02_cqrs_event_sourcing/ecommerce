package org.eyo.architecture.hexagonal.ecommerce.product.application.service;

import org.eyo.architecture.hexagonal.ecommerce.product.domain.dto.CreateProductRequestDTO;
import org.eyo.architecture.hexagonal.ecommerce.product.domain.dto.ProductDTO;
import org.eyo.architecture.hexagonal.ecommerce.product.domain.port.ProductUseCase;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

@Service
public class ProductCommandServiceImpl implements ProductCommandService {
    private ProductUseCase productUseCase;
    private final ApplicationEventPublisher applicationEventPublisher;

    public ProductCommandServiceImpl(ProductUseCase productUseCase, ApplicationEventPublisher applicationEventPublisher) {
        this.applicationEventPublisher = applicationEventPublisher;
        this.productUseCase = productUseCase;
    }

    @Override
    public void create(CreateProductRequestDTO createProductRequestDTO) {
        this.applicationEventPublisher.publishEvent(createProductRequestDTO);
    }

    @Override
    public ProductDTO deleteProduct(Long productId) {
        if (this.productUseCase.getProduct(productId) == null){
            return null;
        }
        this.applicationEventPublisher.publishEvent(new DeleteProductDTO(productId));
        return new ProductDTO();
    }

}
