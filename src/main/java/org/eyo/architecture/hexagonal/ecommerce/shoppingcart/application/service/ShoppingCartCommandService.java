package org.eyo.architecture.hexagonal.ecommerce.shoppingcart.application.service;

import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.dto.CreateShoppingCartDTO;
import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.dto.ShoppingCartDTO;

public interface ShoppingCartCommandService {
    ShoppingCartDTO createShoppingCart(CreateShoppingCartDTO shoppingCartInput);

    ShoppingCartDTO endCart(Long cartId);

    ShoppingCartDTO addProductToShoppingCart(Long cartId, Long productId, Integer quantity);

    ShoppingCartDTO deleteProductFromCart(Long cartId, Long productId);

    ShoppingCartDTO deleteCart(Long cartId);
}
