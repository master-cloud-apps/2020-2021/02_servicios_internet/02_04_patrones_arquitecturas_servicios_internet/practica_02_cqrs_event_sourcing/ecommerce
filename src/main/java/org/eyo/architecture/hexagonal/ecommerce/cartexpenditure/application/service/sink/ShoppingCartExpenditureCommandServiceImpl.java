package org.eyo.architecture.hexagonal.ecommerce.cartexpenditure.application.service.sink;

import org.eyo.architecture.hexagonal.ecommerce.cartexpenditure.domain.port.ShoppingCartExpenditureUseCase;
import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.dto.ShoppingCartDTO;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

@Service
public class ShoppingCartExpenditureCommandServiceImpl implements ShoppingCartExpenditureCommandService {

    private ShoppingCartExpenditureUseCase shoppingCartExpenditureUseCase;

    public ShoppingCartExpenditureCommandServiceImpl(ShoppingCartExpenditureUseCase shoppingCartExpenditureUseCase) {
        this.shoppingCartExpenditureUseCase = shoppingCartExpenditureUseCase;
    }

    @EventListener
    public void saveShoppingCartExpenditure(ShoppingCartDTO endedCart) {
        this.shoppingCartExpenditureUseCase.save(endedCart);
    }
}
