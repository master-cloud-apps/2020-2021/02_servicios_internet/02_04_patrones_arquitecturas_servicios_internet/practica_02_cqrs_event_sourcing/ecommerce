package org.eyo.architecture.hexagonal.ecommerce.cartexpenditure.application.service;

import org.eyo.architecture.hexagonal.ecommerce.cartexpenditure.domain.dto.ShoppingCartExpenditureDTO;

import java.util.List;

public interface ShoppingCartExpenditureQueryService {
    List<ShoppingCartExpenditureDTO> getExpenditures();
}
