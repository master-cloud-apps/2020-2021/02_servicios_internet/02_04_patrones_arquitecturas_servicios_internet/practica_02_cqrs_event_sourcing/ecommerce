package org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.port;

import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.dto.ShoppingCartDTO;

public interface EcommerceManager {
    Boolean validateCart(ShoppingCartDTO shoppingCart);
}
