package org.eyo.architecture.hexagonal.ecommerce.product.application.controller;

import org.eyo.architecture.hexagonal.ecommerce.product.application.service.ProductCommandService;
import org.eyo.architecture.hexagonal.ecommerce.product.domain.dto.CreateProductRequestDTO;
import org.eyo.architecture.hexagonal.ecommerce.product.domain.dto.ProductDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;

import static org.eyo.architecture.hexagonal.ecommerce.utils.AppConstants.PRODUCT_URL;
import static org.springframework.web.servlet.support.ServletUriComponentsBuilder.fromCurrentRequest;

@RestController
@RequestMapping(PRODUCT_URL)
public class ProductCommandController {

    private final ProductCommandService productCommandService;

    public ProductCommandController(ProductCommandService productCommandService) {
        this.productCommandService = productCommandService;
    }

    @PostMapping("")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Void> createProduct(@RequestBody CreateProductRequestDTO productRequest){
        this.productCommandService.create(productRequest);
        URI location = fromCurrentRequest().path("/{id}").buildAndExpand(productRequest.getId()).toUri();
        return ResponseEntity.created(location).build();
    }

    @DeleteMapping("/{productId}")
    public ResponseEntity<Void> deleteProductById(@PathVariable Long productId) {
        ProductDTO productToDelete = this.productCommandService.deleteProduct(productId);
        if (productToDelete == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.noContent().build();
    }


}
