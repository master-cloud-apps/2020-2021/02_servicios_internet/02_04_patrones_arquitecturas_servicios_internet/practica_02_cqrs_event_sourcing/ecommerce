package org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain;

import org.eyo.architecture.hexagonal.ecommerce.product.domain.dto.CreateProductRequestDTO;
import org.eyo.architecture.hexagonal.ecommerce.product.domain.dto.ProductDTO;
import org.eyo.architecture.hexagonal.ecommerce.product.domain.port.ProductUseCase;
import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.dto.CreateShoppingCartDTO;
import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.dto.ShoppingCartDTO;
import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.port.ShoppingCartUseCase;

import static org.eyo.architecture.hexagonal.ecommerce.TestConstants.*;

public class ShoppingCartUseCaseUtils {

    private ShoppingCartUseCase shoppingCartUseCase;
    private ProductUseCase productUseCase;

    public ShoppingCartUseCaseUtils(ShoppingCartUseCase shoppingCartUseCase, ProductUseCase productUseCase) {
        this.shoppingCartUseCase = shoppingCartUseCase;
        this.productUseCase = productUseCase;
    }

    public ShoppingCartDTO createEmptyShoppingCart() {
        CreateShoppingCartDTO shoppingCartInput = new CreateShoppingCartDTO();
        return this.shoppingCartUseCase.createShoppingCart(shoppingCartInput);
    }

    public ProductDTO createProduct() {
        return this.productUseCase.create(new CreateProductRequestDTO(BOOK_KIND, BOOK_NAME, BOOK_DESCRIPTION, BOOK_PRICE));
    }
}
