package org.eyo.architecture.hexagonal.ecommerce;

public class TestConstants {
    public static final String BOOK_DESCRIPTION = "Gabo book";
    public static final String BOOK_NAME = "El Coronel no tiene quien le escriba";
    public static final String BOOK_KIND = "Book";
    public static final double BOOK_PRICE = 12.0;

}
