package org.eyo.architecture.hexagonal.ecommerce.product.application.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.eyo.architecture.hexagonal.ecommerce.product.domain.dto.CreateProductRequestDTO;
import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.dto.CreateShoppingCartDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.eyo.architecture.hexagonal.ecommerce.utils.AppConstants.PRODUCT_URL;
import static org.eyo.architecture.hexagonal.ecommerce.utils.AppConstants.SHOPPING_CART_URL;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Component
public class ControllerUtils {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    protected ObjectMapper objectMapper;

    public Long getIdFromLocation(String locationHeader) {
        return Long.parseLong(locationHeader.substring(locationHeader.lastIndexOf('/') + 1));
    }

    public Long createProduct(CreateProductRequestDTO productRequest) throws Exception {
        MvcResult result = this.mockMvc.perform(post(PRODUCT_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(this.objectMapper.writeValueAsString(productRequest)))
                .andExpect(status().isCreated())
                .andReturn();
        return this.getIdFromLocation(result.getResponse().getHeader("location"));
    }

    public Long createShoppingCart(CreateShoppingCartDTO shoppingCartDTO) throws Exception {
        MvcResult result = this.mockMvc.perform(post(SHOPPING_CART_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(this.objectMapper.writeValueAsString(shoppingCartDTO)))
                .andExpect(status().isCreated())
                .andReturn();
        return this.getIdFromLocation(result.getResponse().getHeader("location"));
    }

}
