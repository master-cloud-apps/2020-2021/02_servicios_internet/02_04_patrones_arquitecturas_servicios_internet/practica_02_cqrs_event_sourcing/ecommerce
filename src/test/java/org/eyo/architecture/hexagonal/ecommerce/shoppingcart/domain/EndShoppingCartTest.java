package org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain;

import org.eyo.architecture.hexagonal.ecommerce.product.domain.ProductUseCaseImpl;
import org.eyo.architecture.hexagonal.ecommerce.product.domain.dto.ProductDTO;
import org.eyo.architecture.hexagonal.ecommerce.product.domain.port.ProductRepository;
import org.eyo.architecture.hexagonal.ecommerce.product.domain.port.ProductUseCase;
import org.eyo.architecture.hexagonal.ecommerce.product.infraestructure.repository.ProductMockRepository;
import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.application.service.EcommerceMockManager;
import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.dto.ShoppingCartDTO;
import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.port.EcommerceManager;
import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.port.ShoppingCartRepository;
import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.port.ShoppingCartUseCase;
import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.infraestructure.repository.ShoppingCartMockRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class EndShoppingCartTest {

    private ShoppingCartUseCase shoppingCartUseCase;
    private ProductUseCase productUseCase;
    private ProductRepository productRepository;
    private ShoppingCartRepository shoppingCartRepository;
    private ShoppingCartUseCaseUtils shoppingCartUseCaseUtils;
    private ShoppingCartDTO cart;
    private ProductDTO bookProduct;

    @BeforeEach
    void setUp() {
        this.shoppingCartRepository = new ShoppingCartMockRepository();
        this.productRepository = new ProductMockRepository();
        this.shoppingCartUseCase = new ShoppingCartUseCaseImpl(this.shoppingCartRepository, this.productRepository,
                new EcommerceMockManager());
        this.productUseCase = new ProductUseCaseImpl(this.productRepository);
        this.shoppingCartUseCaseUtils = new ShoppingCartUseCaseUtils(this.shoppingCartUseCase,
                this.productUseCase);
        this.cart = this.shoppingCartUseCaseUtils.createEmptyShoppingCart();
        this.bookProduct = this.shoppingCartUseCaseUtils.createProduct();
        this.shoppingCartUseCase.addProductToShoppingCart(this.cart.getId(),
                this.bookProduct.getId(), 10);
    }

    @Test
    void givenNotEmptyCartOneItem_whenEndCart_shouldReturnNotValidated(){
        ShoppingCartDTO endedCart = this.shoppingCartUseCase.endCart(this.cart.getId());

        assertEquals(ShoppingCartStatus.NOT_VALIDATED.name(), endedCart.getStatus());
    }

    @Test
    void givenNotEmptyCartTwoItems_whenEndCart_shouldReturnCompleted(){
        ProductDTO product2 = this.shoppingCartUseCaseUtils.createProduct();
        this.shoppingCartUseCase.addProductToShoppingCart(this.cart.getId(),
                product2.getId(), 10);

        ShoppingCartDTO endedCart = this.shoppingCartUseCase.endCart(this.cart.getId());

        assertEquals(ShoppingCartStatus.COMPLETED.name(), endedCart.getStatus());
    }

    @Test
    void givenCartCompleted_whenEndCart_shouldNotIncludeNewProducts(){
        ProductDTO product2 = this.shoppingCartUseCaseUtils.createProduct();
        this.shoppingCartUseCase.addProductToShoppingCart(this.cart.getId(),
                product2.getId(), 10);
        ShoppingCartDTO endedCart = this.shoppingCartUseCase.endCart(this.cart.getId());
        ProductDTO product3 = this.shoppingCartUseCaseUtils.createProduct();
        this.shoppingCartUseCase.addProductToShoppingCart(this.cart.getId(),
                product3.getId(), 10);
        ProductDTO product4 = this.shoppingCartUseCaseUtils.createProduct();
        this.shoppingCartUseCase.addProductToShoppingCart(this.cart.getId(),
                product4.getId(), 10);
        endedCart = this.shoppingCartUseCase.endCart(this.cart.getId());

        assertEquals(ShoppingCartStatus.COMPLETED.name(), endedCart.getStatus());
        assertEquals(2, endedCart.getCartItems().size());
    }

    @Test
    void givenValidatorReturnFalse_whenValidate_shouldReturnNotValidated(){
        ShoppingCart cart = new ShoppingCart();

        ShoppingCartStatus finalStatus = cart.validate(shoppingCart -> Boolean.FALSE);

        assertEquals(ShoppingCartStatus.NOT_VALIDATED, finalStatus);
        assertEquals(ShoppingCartStatus.NOT_VALIDATED, cart.getStatus());
    }

    @Test
    void givenValidatorReturnTrue_whenValidate_shouldReturnCompleted(){
        ShoppingCart cart = new ShoppingCart();

        ShoppingCartStatus finalStatus = cart.validate(shoppingCart -> Boolean.TRUE);

        assertEquals(ShoppingCartStatus.COMPLETED, finalStatus);
        assertEquals(ShoppingCartStatus.COMPLETED, cart.getStatus());
    }
}
