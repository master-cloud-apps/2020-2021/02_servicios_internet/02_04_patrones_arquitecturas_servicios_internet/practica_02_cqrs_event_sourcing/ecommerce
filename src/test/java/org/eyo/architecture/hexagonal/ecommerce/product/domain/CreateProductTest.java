package org.eyo.architecture.hexagonal.ecommerce.product.domain;

import org.eyo.architecture.hexagonal.ecommerce.product.domain.port.ProductUseCase;
import org.eyo.architecture.hexagonal.ecommerce.product.domain.dto.CreateProductRequestDTO;
import org.eyo.architecture.hexagonal.ecommerce.product.domain.dto.ProductDTO;
import org.eyo.architecture.hexagonal.ecommerce.product.infraestructure.repository.ProductMockRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.eyo.architecture.hexagonal.ecommerce.TestConstants.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class CreateProductTest {

    private ProductUseCase productUseCase;

    @BeforeEach
    void setUp() {
        this.productUseCase = new ProductUseCaseImpl(new ProductMockRepository());
    }

    @Test
    void givenProductToCreateEmpty_whenCreateUseCase_shouldReturnNewProductResponseWithId() {
        CreateProductRequestDTO productToCreate = new CreateProductRequestDTO();

        ProductDTO productResponseDTO = this.productUseCase.create(productToCreate);

        assertNotNull(productResponseDTO);
        assertNotNull(productResponseDTO.getId());
        assertNotNull(this.productUseCase.getProduct(productResponseDTO.getId()));
    }

    @Test
    void givenProductToCreateExampleBook_whenCreateUseCase_shouldReturnNewProductBookWithId() {
        CreateProductRequestDTO productToCreate = new CreateProductRequestDTO(BOOK_KIND, BOOK_NAME, BOOK_DESCRIPTION,
                BOOK_PRICE);

        ProductDTO productResponseDTO = this.productUseCase.create(productToCreate);

        assertNotNull(productResponseDTO);
        assertNotNull(productResponseDTO.getId());
        assertEquals(BOOK_KIND, productResponseDTO.getKind());
        assertEquals(BOOK_NAME, productResponseDTO.getName());
        assertEquals(BOOK_DESCRIPTION, productResponseDTO.getDescription());
        assertEquals(BOOK_PRICE, productResponseDTO.getPrice());
    }
}
