package org.eyo.architecture.hexagonal.ecommerce.shoppingcart.application.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.eyo.architecture.hexagonal.ecommerce.cartexpenditure.infraestructure.repository.ShoppingCartExpenditureJpaRepository;
import org.eyo.architecture.hexagonal.ecommerce.product.application.controller.ControllerUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
abstract class TestCartController {

    public static final String PENDING = "PENDING";
    @Autowired
    protected ObjectMapper objectMapper;
    @Autowired
    protected MockMvc mockMvc;
    @Autowired
    protected ControllerUtils controllerUtils;
    @Autowired
    protected ShoppingCartExpenditureJpaRepository shoppingCartExpenditureJpaRepository;
}
