package org.eyo.architecture.hexagonal.ecommerce.product.application.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.eyo.architecture.hexagonal.ecommerce.product.domain.dto.CreateProductRequestDTO;
import org.eyo.architecture.hexagonal.ecommerce.utils.AppConstants;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.eyo.architecture.hexagonal.ecommerce.TestConstants.*;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class ProductCommandControllerTest {

    @Autowired
    protected ObjectMapper objectMapper;
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ControllerUtils controllerUtils;


    @Test
    void shouldValidateCrud() throws Exception {
        Long productId = this.createProduct(new CreateProductRequestDTO(BOOK_KIND, BOOK_NAME, BOOK_DESCRIPTION, BOOK_PRICE));

        this.mockMvc.perform(get(AppConstants.PRODUCT_URL + "/" + productId)).andExpect(status().isOk());
        this.mockMvc.perform(delete(AppConstants.PRODUCT_URL + "/" + productId)).andExpect(status().isNoContent());
        this.mockMvc.perform(get(AppConstants.PRODUCT_URL + "/" + productId)).andExpect(status().isNotFound());
    }

    @Test
    void given_1_book_created_when_calling_books_should_return_list_with_one_element() throws Exception {
        Long productId = this.createProduct(new CreateProductRequestDTO(BOOK_KIND, BOOK_NAME, BOOK_DESCRIPTION));

        this.mockMvc.perform(get(AppConstants.PRODUCT_URL))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].kind").value(BOOK_KIND))
                .andExpect(MockMvcResultMatchers.jsonPath("$.*").isArray());


        this.mockMvc.perform(delete(AppConstants.PRODUCT_URL + productId));
    }

    @Test
    void givenProductCreatedWithPrice_whenGetProduct_shouldShowThePrice() throws Exception {
        Long productId = this.createProduct(new CreateProductRequestDTO(BOOK_KIND, BOOK_NAME, BOOK_DESCRIPTION, BOOK_PRICE));

        this.mockMvc.perform(get(AppConstants.PRODUCT_URL + "/" + productId))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.kind").value(BOOK_KIND))
                .andExpect(MockMvcResultMatchers.jsonPath("$.price").value(BOOK_PRICE));


        this.mockMvc.perform(delete(AppConstants.PRODUCT_URL + productId));
    }

    private Long createProduct(CreateProductRequestDTO productRequest) throws Exception {
        MvcResult result = this.mockMvc.perform(post(AppConstants.PRODUCT_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(this.objectMapper.writeValueAsString(productRequest)))
                .andExpect(status().isCreated())
                .andReturn();
        return this.controllerUtils.getIdFromLocation(result.getResponse().getHeader("location"));
    }
}
