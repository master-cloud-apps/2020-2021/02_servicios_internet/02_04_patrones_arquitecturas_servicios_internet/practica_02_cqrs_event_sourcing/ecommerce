package org.eyo.architecture.hexagonal.ecommerce.shoppingcart.application.controller;


import org.eyo.architecture.hexagonal.ecommerce.product.domain.dto.CreateProductRequestDTO;
import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.dto.CreateShoppingCartDTO;
import org.junit.jupiter.api.Test;

import static org.eyo.architecture.hexagonal.ecommerce.TestConstants.*;
import static org.eyo.architecture.hexagonal.ecommerce.utils.AppConstants.PRODUCT_URL;
import static org.eyo.architecture.hexagonal.ecommerce.utils.AppConstants.SHOPPING_CART_URL;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class ShoppingCartAddProductsTest extends TestCartController{

    @Test
    void shouldAddProductsInCart() throws Exception {
        Long cartId = this.controllerUtils.createShoppingCart(new CreateShoppingCartDTO(PENDING));
        Long productId = this.controllerUtils.createProduct(new CreateProductRequestDTO(BOOK_KIND, BOOK_NAME,
                BOOK_DESCRIPTION));

        this.mockMvc.perform(post(SHOPPING_CART_URL + "/" + cartId + "/product/" + productId +
                "/quantity/10")).andExpect(status().isOk());

        this.mockMvc.perform(delete(SHOPPING_CART_URL + "/" + cartId)).andExpect(status().isNoContent());
        this.mockMvc.perform(delete(PRODUCT_URL + productId));
    }

    @Test
    void shouldAddTwoProductsInCart() throws Exception {
        Long cartId = this.controllerUtils.createShoppingCart(new CreateShoppingCartDTO(PENDING));
        Long productId = this.controllerUtils.createProduct(new CreateProductRequestDTO(BOOK_KIND, BOOK_NAME,
                BOOK_DESCRIPTION));
        Long productId_2 = this.controllerUtils.createProduct(new CreateProductRequestDTO(BOOK_KIND, BOOK_NAME,
                BOOK_DESCRIPTION));

        this.mockMvc.perform(post(SHOPPING_CART_URL + "/" + cartId + "/product/" + productId +
                "/quantity/10")).andExpect(status().isOk());
        this.mockMvc.perform(post(SHOPPING_CART_URL + "/" + cartId + "/product/" + productId_2 +
                "/quantity/10")).andExpect(status().isOk());

        this.mockMvc.perform(delete(SHOPPING_CART_URL + "/" + cartId)).andExpect(status().isNoContent());
        this.mockMvc.perform(delete(PRODUCT_URL + productId));
        this.mockMvc.perform(delete(PRODUCT_URL + productId_2));
    }

    @Test
    void shouldAddTwoProductsInCartAndThenRemoveOne() throws Exception {
        Long cartId = this.controllerUtils.createShoppingCart(new CreateShoppingCartDTO(PENDING));
        Long productId = this.controllerUtils.createProduct(new CreateProductRequestDTO(BOOK_KIND, BOOK_NAME,
                BOOK_DESCRIPTION));
        Long productId_2 = this.controllerUtils.createProduct(new CreateProductRequestDTO(BOOK_KIND, BOOK_NAME,
                BOOK_DESCRIPTION));

        this.mockMvc.perform(post(SHOPPING_CART_URL + "/" + cartId + "/product/" + productId +
                "/quantity/10"))
                .andExpect(status().isOk());
        this.mockMvc.perform(post(SHOPPING_CART_URL + "/" + cartId + "/product/" + productId_2 +
                "/quantity/10"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.cartItems", hasSize(2)));
        this.mockMvc.perform(delete(SHOPPING_CART_URL + "/" + cartId + "/product/" + productId_2))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.cartItems", hasSize(1)));

        this.mockMvc.perform(delete(SHOPPING_CART_URL + "/" + cartId)).andExpect(status().isNoContent());
        this.mockMvc.perform(delete(PRODUCT_URL + productId));
        this.mockMvc.perform(delete(PRODUCT_URL + productId_2));
    }

    @Test
    void givenCartWithOneProduct_whenDeleteNotExistingCartProduct_shouldReturnNotFound() throws Exception {
        this.mockMvc.perform(delete(SHOPPING_CART_URL + "/100/product/0"))
                .andExpect(status().isNotFound());
    }

    @Test
    void shouldReturnNotFounds() throws Exception {
        Long cartId = this.controllerUtils.createShoppingCart(new CreateShoppingCartDTO(PENDING));
        Long productId = this.controllerUtils.createProduct(new CreateProductRequestDTO(BOOK_KIND, BOOK_NAME,
                BOOK_DESCRIPTION));

        this.mockMvc.perform(post(SHOPPING_CART_URL + "/" + 100L + "/product/" + productId +
                "/quantity/10")).andExpect(status().isNotFound());
        this.mockMvc.perform(post(SHOPPING_CART_URL + "/" + cartId + "/product/" + 100L +
                "/quantity/10")).andExpect(status().isOk()).andExpect(jsonPath("$.cartItems", hasSize(0)));

        this.mockMvc.perform(delete(SHOPPING_CART_URL + "/" + cartId)).andExpect(status().isNoContent());
        this.mockMvc.perform(delete(PRODUCT_URL + productId));
    }



}
