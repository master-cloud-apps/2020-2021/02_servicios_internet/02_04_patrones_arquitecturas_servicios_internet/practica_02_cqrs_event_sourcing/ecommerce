package org.eyo.architecture.hexagonal.ecommerce.shoppingcart.application.controller;


import org.eyo.architecture.hexagonal.ecommerce.product.domain.dto.CreateProductRequestDTO;
import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.ShoppingCartStatus;
import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.dto.CreateShoppingCartDTO;
import org.junit.jupiter.api.Test;

import static org.eyo.architecture.hexagonal.ecommerce.TestConstants.*;
import static org.eyo.architecture.hexagonal.ecommerce.utils.AppConstants.PRODUCT_URL;
import static org.eyo.architecture.hexagonal.ecommerce.utils.AppConstants.SHOPPING_CART_URL;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


class ShoppingCartEndCartTest extends TestCartController {


    @Test
    void shouldEndCart() throws Exception {
        Long cartId = this.controllerUtils.createShoppingCart(new CreateShoppingCartDTO(PENDING));
        Long productId = this.controllerUtils.createProduct(new CreateProductRequestDTO(BOOK_KIND, BOOK_NAME,
                BOOK_DESCRIPTION));
        Long productId_2 = this.controllerUtils.createProduct(new CreateProductRequestDTO(BOOK_KIND, BOOK_NAME,
                BOOK_DESCRIPTION));

        this.mockMvc.perform(post(SHOPPING_CART_URL + "/" + cartId + "/product/" + productId +
                "/quantity/10")).andExpect(status().isOk());
        this.mockMvc.perform(post(SHOPPING_CART_URL + "/" + cartId + "/product/" + productId_2 +
                "/quantity/10")).andExpect(status().isOk());

        this.mockMvc.perform(patch(SHOPPING_CART_URL + "/" + cartId))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value(ShoppingCartStatus.COMPLETED.name()));

        this.shoppingCartExpenditureJpaRepository.deleteAll();
        this.mockMvc.perform(delete(SHOPPING_CART_URL + "/" + cartId)).andExpect(status().isNoContent());
        this.mockMvc.perform(delete(PRODUCT_URL + productId));
        this.mockMvc.perform(delete(PRODUCT_URL + productId_2));
    }

    @Test
    void givenCartOneProduct_shouldNotEndCart() throws Exception {
        Long cartId = this.controllerUtils.createShoppingCart(new CreateShoppingCartDTO("PENDING"));
        Long productId = this.controllerUtils.createProduct(new CreateProductRequestDTO(BOOK_KIND, BOOK_NAME,
                BOOK_DESCRIPTION));

        this.mockMvc.perform(post(SHOPPING_CART_URL + "/" + cartId + "/product/" + productId +
                "/quantity/10")).andExpect(status().isOk());

        this.mockMvc.perform(patch(SHOPPING_CART_URL + "/" + cartId))
                .andExpect(status().isBadRequest());

        this.shoppingCartExpenditureJpaRepository.deleteAll();
        this.mockMvc.perform(delete(SHOPPING_CART_URL + "/" + cartId)).andExpect(status().isNoContent());
        this.mockMvc.perform(delete(PRODUCT_URL + productId));
    }

    @Test
    void whenEndNotExistingCart_shouldReturnNotFound() throws Exception {
        this.mockMvc.perform(patch(SHOPPING_CART_URL + "/100"))
                .andExpect(status().isNotFound());
    }

}
